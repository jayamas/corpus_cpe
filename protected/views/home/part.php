<div class="cover" style="background-image: url('<?php echo Yii::app()->baseUrl.'/asset/images/ill-partner-cpe.jpg'; ?>');">
	<div class="text"><h2>Our Partner</h2></div>
</div>

<section class="breadcrumb-insides">
	<div class="prelative container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">Our Partner</a></li>
		  </ol>
		  <div class="back float-right">
		  	<a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><span><img src="<?php echo $this->assetBaseurl; ?>arrow-back.png" alt=""></span>BACK TO PREVIOUS PAGE</a>
		  </div>
		</nav>
	</div>
</section>

<section class="part-sec-1">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-60">
				<div class="title">
					<p>We maintain strategic relationship with our partners</p>
				</div>
				<div class="image"><img src="<?php echo $this->assetBaseurl; ?>kkkkk.png" alt=""></div>
				<div class="our-corp">
					<p>Our Cooperation Partner</p>
				</div>
				<div class="above-our-corp">
					<p>PT. Telekomunikasi Indonesia Tbk, PT. Waskita Karya Tbk, PT. Wijaya Karya Tbk, PT. Haka Aston, dan lain lain.</p>
				</div>
			</div>
		</div>
	</div>
</section>
