<section class="home-sec-1">
	<div class="prelative container">
		<div class="box-content">
			<div class="row">
				<div class="col-md-30">
					<div class="title">
						<h4>We have dealt with the best player in the industry cause we only bring the best to our customers.</h4>
					</div>
					<div class="subtitle">
						<?php $ln_contact = CHtml::normalizeUrl(array('/home/contactus', 'lang'=>Yii::app()->language)); ?>
						<p>To meet customer needs, PT Corpus Prima Energi holds a strong relationship with:
							PT EXXON Mobil Lubricants Indonesia, British Petroleum, Gunvor, Hin Leong, HinHin, Pertamina Group.
							We are a fuel distribution chain with oil products that complies with safety standards required.
							Let us help you with your fuel supply - contact us to discuss your needs.</p>
					</div>
				</div>
				<div class="col-md-30">
					<div class="tombol">
						<a href="<?php echo $this->setting['home_section1_link_schedule'] ?>"><?php echo $this->setting['home_section1_link_schedule_text'] ?></a>
						<p><?php echo $this->setting['home_section1_link_schedule_text_info'] ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--<section class="outers-box-videos">-->
<!--	<div class="box-videos-nyoutubes">-->
<!--		<div class="prelatife container">-->
<!--			<div class="inner-video">-->
<!--				<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/xh4k0oZBoDc' frameborder='0' allowfullscreen></iframe></div>-->
<!--				<div class="clear"></div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</section>-->

<section class="home-sec-2" id="div1">

	<div class="prelative container">
		<div class="box-content">
			<div class="row">
				<div class="col-md-60">
					<img src="<?php echo $this->assetBaseurl; ?>logo-sec-2.png" alt="">
					<div class="title">
						<p>YOUR PARTNER IN FUEL SUPPLY</p>
					</div>
					<div class="sub">
						<p>Why Choose Corpus Prima Energy</p>
					</div>
					<div class="content">
						PT. Corpus Prima Energy is committed to providing best quality fuels with best service to the customers. Our commitment to our customers is to assure service satisfaction and to build trust for lasting relationship. <br>
						<strong>Reliable - Consistent - Trusted</strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="home-sec-our d-none d-sm-block">
	<div class="prelative container">
		<div class="box-our-service">
			<div class="title">
				<p><?php echo Tt::t('front', 'Our Fuel Product Partners') ?></p>
			</div>
			<div class="box-content-outer">
				<div class="box-content-inner">
					<div class="row">
						<div class="col-md-60">
							<div class="box-isi-content">
								<img src="<?php echo $this->assetBaseurl; ?>Layer-36.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-cont">
				<a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>" target="_blank" rel=""><?php echo Tt::t('front', 'Learn More About Our Products'); ?></a>
				<div class="py-4"></div>
			</div>
		</div>
	</div>
</section>

<section class="home-sec-our d-block d-sm-none">
	<div class="prelative container">
		<div class="box-our-service">
			<div class="title">
				<p><?php echo Tt::t('front', 'Our Fuel Product Partners') ?></p>
			</div>
			<div class="box-content-outer">
				<div class="box-content-inner">
					<div class="row">
						<div class="col-md-60">
							<div class="box-isi-content">
								<img src="<?php echo $this->assetBaseurl; ?>Layer-36-mob.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-cont">
				<a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>" target="_blank" rel=""><?php echo Tt::t('front', 'Learn More About Our Products'); ?></a>
				<div class="py-4"></div>
			</div>
		</div>
	</div>
</section>

<section class="home-sec-3">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-60">
				<div class="clear"></div>
			</div>
		</div>
	</div>
</section>

<!--<section class="home-sec-4">-->
<!--	<div class="prelative container">-->
<!--		<div class="box-header">-->
<!--			<div class="row">-->
<!--				<div class="col-md-60">-->
<!--					<div class="title">-->
<!--						<p>--><?php //echo $this->setting['home_section2_smalltitle'] ?><!--</p>-->
<!--					</div>-->
<!--					<div class="sub">-->
<!--						<p>--><?php //echo $this->setting['home_section2_title'] ?><!--</p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--		<div class="box-row-content">-->
<!--			<div class="row">-->
<!--				--><?php //foreach ($dataBlog->getData() as $key => $value): ?>
<!--	            <div class="col-md-20">-->
<!--	                <div class="box-content-inner">-->
<!--	                    <div class="image">-->
<!--	                        <a href="--><?php //echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?><!--">-->
<!--	                            <img class="w-100" src="--><?php //echo Yii::app()->baseUrl.ImageHelper::thumb(403,268, '/images/blog/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?><!--" alt="">-->
<!--	                        </a>-->
<!--	                    </div>-->
<!--	                    <div class="tanggal">-->
<!--	                        <p><i class="fa fa-calendar"></i> &nbsp;--><?php //echo date("d F Y", strtotime($value->date_input)); ?><!--</p>-->
<!--	                    </div>-->
<!--	                    <div class="content">-->
<!--	                        <a href="--><?php //echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?><!--">-->
<!--	                            <p>--><?php //echo $value->description->title ?><!--</p>-->
<!--	                        </a>-->
<!--	                    </div>-->
<!--	                    <div class="read-more">-->
<!--	                        <a href="--><?php //echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id, 'lang'=>Yii::app()->language)); ?><!--">Read More</a>-->
<!--	                    </div>-->
<!--	                </div>-->
<!--	            </div>-->
<!--				--><?php //endforeach ?>
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--	<div class="pb-5"></div>-->
<!--	<div class="pb-5 d-none d-sm-block"></div>-->
<!--</section>-->
