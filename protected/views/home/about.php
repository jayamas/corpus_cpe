<div class="cover" style="background-image: url('<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['about_hero_image']; ?>');">
  <div class="text"><h2><?php echo $this->setting['about_hero_title'] ?></h2></div>
</div>

<section class="breadcrumb-insides">
	<div class="prelative container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>"><?php echo $this->setting['about_hero_title'] ?></a></li>
		  </ol>
		  <div class="back float-right">
		  	<a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><span><img src="<?php echo $this->assetBaseurl; ?>arrow-back.png" alt=""></span>BACK TO PREVIOUS PAGE</a>
		  </div>
		</nav>
	</div>
</section>

<section class="about-sec-1">
	<div class="prelative container">
		<div class="box-container">
			<div class="row">
				<div class="col-md-35">
					<div class="box-content">
						<div class="title">
							<h4><?php echo $this->setting['about2_small_title'] ?></h4>
						</div>
						<div class="sub">
							<p>Starting with the joint commitment of its founders, PT Corpus Prima Energi is confident of being able to develop in quality, integrity and professionalism in its fields.</p>
						</div>
						<div class="content">
							<p>We believe our company will provide the best solution for our clients. Previously, our business group was a partner for a well-known consulting company in Jakarta. Our clients are mostly based in East Java and they came from various businesses. Our next big step step would be to expand our services in the field of fuel oil trading, widely spread throughout the country of Indonesia.</p>
							<p>PT. Corpus Prima Energi provides services for refueling oil in the domestic industry, mining and shipping and internationally, including fishing vessel companies. This business is carried out in order to meet opportunities for needs industrial diesel oil (HSD) which in the initial stages focuses on state-owned SOEs, so that it can eventually meet the needs of Java, Kalimantan and Sulawesi even to Papua. Due to the big demand from large industries, we have also added Marine Fuel Oil (MFO) to the service line of Corpus Prima Energy.</p>
						</div>
					</div>
				</div>
				<div class="col-md-25w">
					<div class="image"><img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['about2_pictures']; ?>" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--<section class="about-sec-2">-->
<!--	<div class="prelative container py-5">-->
<!--		<div class="pt-5">	</div>-->
<!--		<div class="box-outer">-->
<!--			<div class="row no-gutters">-->
<!--				<div class="col-md-30">-->
<!--					<div class="box-left-1">-->
<!--						<div class="content">-->
<!--							<div class="title">-->
<!--								<p>--><?php //echo $this->setting['about3_title_1'] ?><!--</p>-->
<!--							</div>-->
<!--							<div class="isi">-->
<!--								--><?php //echo $this->setting['about3_content_1'] ?>
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--				<div class="col-md-30">-->
<!--					<div class="box-right-1"><img class="w-100 img img-fluid" src="--><?php //echo Yii::app()->baseUrl.'/images/static/'. $this->setting['about3_pictures_1']; ?><!--" alt=""></div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--		<div class="pt-5"></div>-->
<!--		<div class="box-outer">-->
<!--			<div class="row no-gutters">-->
<!--				<div class="col-md-30">-->
<!--					<div class="box-left-2"><img class="w-100 img img-fluid" src="--><?php //echo Yii::app()->baseUrl.'/images/static/'. $this->setting['about3_pictures_2']; ?><!--" alt=""></div>-->
<!--				</div>-->
<!--				<div class="col-md-30">-->
<!--					<div class="box-right-2">-->
<!--						<div class="content">-->
<!--							<div class="title">-->
<!--								<p>--><?php //echo $this->setting['about3_title_2'] ?><!--</p>-->
<!--							</div>-->
<!--							<div class="isi">-->
<!--								--><?php //echo $this->setting['about3_content_2'] ?>
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--		<div class="pb-5"></div>-->
<!--	</div>-->
<!--</section>-->

<section class="about-sec-3">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-30">
				<div class="visi-misi">
					<div class="title">
						<p>Vision</p>
					</div>
					<div class="content">
						<p>Committed to provide unrivaled service satisfaction to customers.</p>
					</div>
				</div>
			</div>
			<div class="col-md-30">
				<div class="py-2 d-block d-sm-none"></div>
				<div class="visi-misi">
					<div class="title">
						<p>Mission</p>
					</div>
					<div class="content">
						<p>Providing quality, competitive and punctual delivery of oil fuel for customer satisfaction</p>
						<p>Playing a big role in our nation's development by expediting the distribution of oil fuel</p>
						<p>To be the leading and the most trusted providers of oil fuel</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php /*
<section class="about-sec-4">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-60">
				<div class="head">
					<div class="title">
						<p><?php echo $this->setting['about5_title'] ?></p>
					</div>
					<div class="sub">
						<p><?php echo $this->setting['about5_subtitle'] ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="prelative container box-about">
		<div class="box-pejabat">
			<div class="row">
				<?php
				$m_team = TeamList::model()->findAll();
				?>
				<?php foreach ($m_team as $key => $value): ?>
				<div class="col-md-12 pb-4">
					<div class="image"><img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/team/'. $value->image; ?>" alt=""></div>
					<div class="nama">
						<p><?php echo $value->title ?></p>
					</div>
					<div class="kat">
						<?php if (Yii::app()->language == 'en'): ?>
						<p><?php echo $value->position_en ?></p>
						<?php else: ?>
						<p><?php echo $value->position_id ?></p>
						<?php endif ?>
					</div>
				</div>
				<?php endforeach ?>
			</div>

		</div>
	</div>
	<div class="pb-5 d-none d-sm-block"></div>
</section>
*/ ?>

<section class="about-sec-5 d-none d-sm-block">
	<div class="prelative container">
		<div class="title-head">
			<p>CORPORATE STRUCTURE</p>
		</div>
		<div class="subtitle-head">
			<p>Corpus Prima Energi Corporate Structure</p>
		</div>
		<div class="image"><img class="mx-auto d-block" src="<?php echo $this->assetBaseurl; ?>cpe-structure.jpg" alt=""></div>
	</div>
</section>
<section class="about-sec-5 d-block d-sm-none">
	<div class="prelative container">
		<div class="title-head">
			<p>CORPORATE STRUCTURE</p>
		</div>
		<div class="subtitle-head">
			<p>Corpus Prima Energi Corporate Structure</p>
		</div>
		<div class="image"><img class="w-100 mx-auto d-block" src="<?php echo $this->assetBaseurl; ?>cpe-structure.jpg" alt=""></div>
	</div>
</section>

<!--<section class="about-sec-6">-->
<!--	<div class="prelative container">-->
<!--		<div class="subtitle-head">-->
<!--			<p>Corpus Organisation Structure</p>-->
<!--		</div>-->
<!--		<div class="image"><img class="w-100" src="--><?php //echo $this->assetBaseurl; ?><!--Layer-27.png" alt=""></div>-->
<!--	</div>-->
<!--</section>-->
